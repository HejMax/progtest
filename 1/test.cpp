#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <map>
#include <vector>
#include <algorithm>
#include <set>
#include <queue>
using namespace std;
#endif /* __PROGTEST__ */

//byte representation of ascii or utf-8 char.
struct TChar
{
  int type = 0;
  unsigned char character[4] = {'\0', '\0', '\0', '\0'};
};

struct TNode
{
  TChar character;
  TNode * parent = NULL;
  TNode * leftSubTree = NULL;
  TNode * rightSubTree = NULL;
};

//this class contains all the file related operations, such as reading writing and parsing
class CBitFile
{
  public:
  // destructor to close file if file throws error
  ~CBitFile() {file.close();}
    bool openRFile(const char * fileName);
    bool openWFile(const char * fileName);
    void closeFile();
    bool putByte(unsigned char byte);
    int getBit();
    bool getChar(TChar &);
    bool getBits(int number, vector<int>& bits);
    string charToString(TChar &);
    vector<unsigned char> createBytes(vector<int>& bits);
    int bitsToInt(vector<int> &bits);
    bool sync();
  private:
    bool getByte();
    const char * name;
    int position;
    char byte;
    fstream file;

};

class CBTree
{
  public:
    bool insertNode();
    TNode * getRoot();
    void removeNode(TNode *);
    // ~CBTree();
  private:
    TNode root;
};

class CHuffmanCode
{
  public:
    CHuffmanCode(const char * sourceFile, const char * targetFile) {source = sourceFile; target = targetFile;};
    bool decompress();
    bool compress();
    bool loader();
    bool saver();
    void printTree(TNode *);
    bool loadTree(TNode *);
    bool parseChunk(int size, int &bit);
  private:
    bool saveTree();
    const char * source;
    const char * target;
    CBitFile readFile;
    CBitFile writeFile;
    CBTree tree;
    vector<unsigned char> message; 
};

bool CBitFile::openRFile(const char * fileName)
{
  file.open(fileName, ios::in | ios::binary );
  if(file.is_open() == false)
  {
    return false;
  }
  getByte();
  return true;
}
bool CBitFile::openWFile(const char * fileName)
{
  file.open(fileName, ios::out | ios::binary );
  if(file.is_open() == false)
  {
    return false;
  }
  return true;
}

bool CBitFile::getByte()
{
  if(!file.eof())
  {
    file.get(byte);
    if(file.fail())
    {
      return false;
    }
    position = 0;
    return true;
  }
  else
  {
    file.close();
    return false;
  }
  return true;
}

bool CBitFile::putByte(unsigned char byte)
{
  file.put(byte);
  if(file.fail() == true)
  {
    return false;
  }
  return true;
}

int CBitFile::getBit()
{
  //if position is eight it means that we need to grab another byte from file.
  if(position == 8)
  {
    //if getByte returns false, than we know that we have reached the end of the file
    //and will return -1 to signal that or that something bad happened with file.
    if(!getByte())
    {
      return -1;
    }
  }
  int bit = ((unsigned char) byte >> (7- position)) & 1;
  position ++;
  return bit;
}

bool CBitFile::getBits(int number, vector<int> &bits)
{
  for(int i = 0; i < number; i++)
  {
    int bit = getBit();
    if(bit == -1)
    {
      return false;
    }
    else
    {
      bits.push_back(bit);
    } 
  }
  return true;
}

bool CBitFile::getChar(TChar & symbol)
{
  vector<int> bits;

  //every time I pull bits from file I check to see if everything came out correctly.
  //Will maybe need to add testing of prefix of all bytes in utf-8 character.

  if(!getBits(8, bits))
  {
    return false;
  }

  if(bits.at(0) == 0)
  {
    symbol.type = 1;
  }
  else if(bits.at(1) == 1)
  {
    if(bits.at(2) == 1)
    {
      if(bits.at(3) == 1)
      {
        symbol.type = 4;
        //get 24 bits
         if(!getBits(16, bits))
        {
          return false;
        }
      }
      else
      {
        symbol.type = 3;
        //get 16 bits
         if(!getBits(16, bits))
        {
          return false;
        }
      }
    }
    else
    {
      symbol.type = 2;
      //get 8 bits
      if(!getBits(8, bits))
      {
        return false;
      }
    }
  }
    vector<unsigned char> temp;
    temp = createBytes(bits);
    for(int i = 0; i < symbol.type; i++)
    {
      symbol.character[i] = temp.at(i);
    }
  return true;
}
int CBitFile::bitsToInt(vector<int> &bits)
{
  int n = 0;
  for (size_t i = 0; i < bits.size(); i++)
  {
    n <<= 1;
    n |= bits.at(i) - 0;
  }
  return n;
}

vector<unsigned char> CBitFile::createBytes(vector<int> &bits)
{
  vector<unsigned char> bytes = {'\0', '\0', '\0', '\0'};
  bytes.resize(bits.size()/8);
  for(unsigned int i = 0; i < bits.size()/8; i++)
  {
    bytes.at(i) = 0;
    for(int j = 0; j < 8; j++)
    {
      if( bits.at(i*8+j) == 1 ) 
      {
        bytes.at(i) |= 1 << (7-j);
      }

    }
  }
  return bytes;
}

string CBitFile::charToString(TChar & character)
{
  string symbol = "\0";
  for(int i = 0; i < 4; i++)
  {
    if(character.character[i] != '\0')
    {
      symbol += character.character[i];
    }
  }
  return symbol;
}
void CBitFile::closeFile()
{
  file.close();
}

bool CBitFile::sync()
{
  file.flush();
  if(file.fail())
  {
    return false;
  }
  return true;
}

bool decompressFile ( const char * inFileName, const char * outFileName )
{
  CHuffmanCode decoder(inFileName, outFileName);

  if(decoder.loader() == false)
  {
    return false;
  }
  if(decoder.decompress() == false)
  {
    return false;
  }
  return true;
}

bool compressFile ( const char * inFileName, const char * outFileName )
{
  // keep this dummy implementation (no bonus) or implement the compression (bonus)
  return false;
}

bool CHuffmanCode::loader()
{
  if(readFile.openRFile(source) == false || writeFile.openWFile(target) == false)
  {
    return false;
  }
  return true;
}

bool CHuffmanCode::decompress()
{
  //rekursive function to load the decompressing tree
  if(loadTree(this -> tree.getRoot()) == false)
  {
    return false;
  }
  int bit = readFile.getBit();

  do
  {
    // cout << bit << endl;
    if(bit == -1)
    {
      return false;
    }
    if(bit == 1)
    {
      // cout << 4096 << endl;
      if(parseChunk(4096, bit) == false)
      {
        return false;
      }
      if(writeFile.sync() == false)
      {
        return false;
      }
    }
    if(bit == 0)
    {
      vector<int> chunkSize;
      if(readFile.getBits(12,chunkSize) == false)
      {
        return false;
      }
      // cout << readFile.bitsToInt(chunkSize) << endl;
      if(parseChunk(readFile.bitsToInt(chunkSize), bit) == false)
      {
        return false;
      }
      
    }
    
  } 
  while (bit == 1);
  readFile.closeFile();
  writeFile.closeFile();
  return true;
}

bool CHuffmanCode::parseChunk(int size, int &bit)
{
  int counter = 0;
  TNode node = *tree.getRoot();
  while(counter < size)
  {
    bit = readFile.getBit();
    if(node.character.type != 0)
    {
      for(int i = 0; i < node.character.type; i++)
      {
        if(writeFile.putByte(node.character.character[i]) == false)
        {
          return false;
        }
      }
      node = *tree.getRoot();
      counter ++;
      if(counter == size)
      {
        return true;
      }
    }
    if(bit == -1 && counter != size)
    {
      return false;
    }
    else if(bit == 0)
    {
      node =  *node.leftSubTree;
    }
    else if(bit == 1)
    {
      node = *node.rightSubTree;
    }
  }
  // cout << bit << endl;
  return true;
}


bool CHuffmanCode::loadTree(TNode * node)
{
  int bit = readFile.getBit();

  if(bit == -1)
  {
    return false;
  }
  if(bit == 0)
  {
    //load tree leftSub tree, load tree rightSUb tree
    TNode * left = new TNode;
    node -> leftSubTree = left;
    left -> parent = node; 
    loadTree(node -> leftSubTree);

    TNode * right = new TNode;
    right -> parent = node;
    node -> rightSubTree = right;

    
    loadTree(node -> rightSubTree);
  }
  else
  {
    if(readFile.getChar(node->character) == false)
    {
      return false;
    }
      
  }
  return true;
}
void CHuffmanCode::printTree(TNode * node)
{
   if (node == NULL) 
        return; 
  
    /* first print data of node */
    if(node -> character.type == 0)
    {
      cout << 0 << endl;
    }
    else
    {
      cout << "\t" << readFile.charToString(node->character)<< endl; 
    }
    
    /* then recur on left sutree */
    printTree(node->leftSubTree);  
  
    /* now recur on right subtree */
    printTree(node->rightSubTree); 
}
TNode * CBTree::getRoot()
{
  return &this -> root;
}
// void CBTree::removeNode(TNode * node)
// {
//   if(node->character.type == 0)
//   {
//     removeNode(node -> leftSubTree);
//     removeNode(node -> rightSubTree);
//   }
//   if(node -> parent != NULL && node -> leftSubTree != NULL && node -> rightSubTree != NULL)
//   {
//     delete node;
//   }
// }
// CBTree::~CBTree()
// {
//   removeNode(&root);
// }

#ifndef __PROGTEST__
bool identicalFiles ( const char * fileName1, const char * fileName2 )
{
  fstream f1;
  fstream f2;
  f1.open(fileName1, ios::in | ios::binary );
  f2.open(fileName2, ios::in | ios::binary );
  char byte1, byte2;
  while (!f1.eof())
  {
    f1.get(byte1);
    f2.get(byte2);
    if(byte1 != byte2)
    {
      return false;
    }
  }
  if(!f2.eof())
  {
    return false;
  }
  f1.close();
  f2.close();
  return true;
}

int main ( void )
{
  //testing reading of chars from file (ascii and utf-8)

  TChar test;
  CBitFile f1;
  // f1.openRFile("tests/ascii.txt");
  // f1.getChar(test);
  // assert(f1.charToString(test).compare("č") == 0);

  // CHuffmanCode t1("tests/test0.huf", "tempfile" );
  // CBTree tree1;
  // t1.loader();
  // t1.loadTree(tree1.getRoot());
  // t1.printTree(tree1.getRoot());


  // assert ( decompressFile ( "tests/test0.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/test0.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/test1.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/test1.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/test2.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/test2.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/test3.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/test3.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/test4.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/test4.orig", "tempfile" ) );

  // assert ( ! decompressFile ( "tests/test5.huf", "tempfile" ) );


  f1.openRFile("progtest/1/tests/ascii.txt");
  f1.getChar(test);
  assert(f1.charToString(test).compare("č") == 0);

  CHuffmanCode t1("progtest/1/tests/test0.huf", "tempfile" );
  CBTree tree1;
  t1.loader();
  t1.loadTree(tree1.getRoot());
  t1.printTree(tree1.getRoot());


  assert ( decompressFile ( "progtest/1/tests/test0.huf", "progtest/1/tempfile" ) );
  assert ( identicalFiles ( "progtest/1/tests/test0.orig", "progtest/1/tempfile" ) );

  assert ( decompressFile ( "progtest/1/tests/test1.huf", "progtest/1/tempfile" ) );
  assert ( identicalFiles ( "progtest/1/tests/test1.orig", "progtest/1/tempfile" ) );

  assert ( decompressFile ( "progtest/1/tests/test2.huf", "progtest/1/tempfile" ) );
  assert ( identicalFiles ( "progtest/1/tests/test2.orig", "progtest/1/tempfile" ) );

  assert ( decompressFile ( "progtest/1/tests/test3.huf", "progtest/1/tempfile" ) );
  assert ( identicalFiles ( "progtest/1/tests/test3.orig", "progtest/1/tempfile" ) );

  assert ( decompressFile ( "progtest/1/tests/test4.huf", "progtest/1/tempfile" ) );
  assert ( identicalFiles ( "progtest/1/tests/test4.orig", "progtest/1/tempfile" ) );

  assert ( ! decompressFile ( "progtest/1/tests/test5.huf", "progtest/1/tempfile" ) );

  assert ( ! decompressFile ( "prgotest/1/tests/pre0.huf",  "progtest/1/tempfile" ) );
  assert ( ! decompressFile ( "prgotest/1/tests/pre1.huf",  "progtest/1/tempfile" ) );
  assert ( ! decompressFile ( "prgotest/1/tests/pre2.huf",  "progtest/1/tempfile" ) );
  
  assert ( decompressFile ( "progtest/1/tests/pre4.huf", "progtest/1/tempfile" ) );
  assert ( identicalFiles ( "progtest/1/tests/pre4.orig", "progtest/1/tempfile" ) );
  // assert ( decompressFile ( "tests/extra0.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra0.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra1.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra1.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra2.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra2.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra3.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra3.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra4.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra4.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra5.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra5.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra6.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra6.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra7.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra7.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra8.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra8.orig", "tempfile" ) );

  // assert ( decompressFile ( "tests/extra9.huf", "tempfile" ) );
  // assert ( identicalFiles ( "tests/extra9.orig", "tempfile" ) );

  return 0;
}
#endif /* __PROGTEST__ */
